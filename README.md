## Système IoT de MAGANTI


### Problématique

Les fournisseurs d’électricité n’offrent pas une solution digitale. Pour calculer la
consommation d’électricité de chaque client, ils envoient des agents physiques à
chaque maison, ces derniers, lisent les compteurs et après envoient les résultats vers
l’entreprise pour les analyser et construire des factures. Après la génération des fac-
teurs, les agents vont les distribuer à chaque maison, c’est un processus coûteux,
n’est-ce pas?

Donc notre solution vise à automatiser ce processus. La solution va remplacer le
rôle des agents physiques. Nous allons concevoir un système intelligent qui capte
les données du capteur de chaque maison, et il traite ces données avec les para-
digmes de la programmation informatique, après elle (la solution) fournie aux
clients les différentes informations nécessaires pour payer sa consommation d’élec-
tricité comme : combien de KW a consommé dans le dernier mois ?, combien d’ar-
gent doit-il payer pour ce mois etc.

### Objectifs du projet

Les objectifs de ce projet sont : fournir une solution digitale aux fournisseurs d’élec-
tricité et ainsi leurs clients, alors nous avons deux parties, la première est celle de
fournisseur et la deuxième est celle des clients.

Nous construisons une plateforme web qui aide les fournisseurs à avoir une vue
globale sur ses clients et leurs consommations. Aussi nous construisons une appli-
cation mobile qui fournit les informations nécessaires aux clients, comme leurs con-
sommations en KWh (mois actuel) et les facteurs qu’ils doivent les payer (mois
actuel).

### Application Mobile : côté clients

Cette application contient 4 onglets, chaque onglet a comme rôle :

```
 Onglet ‘Login’ : permet aux utilisateurs de s’authentifier.
 Onglet ‘Accueil’ : onglet principal qui contient toutes les informations sur
la consommation, courant (a une relation avec le domaine d’électricité), la
quantité consommée aujourd’hui, aussi la quantité consommée du mois ac-
tuel et une rubrique de facturation.
 Onglet ‘Paramètre’ : cet onglet permet de changer le mot de passe de l’uti-
lisateur et aussi la préférence de couleur (mode nuit et mode jour)
 Onglet ‘About’ : donne des informations sur les créateurs du projet ainsi
leurs contacts.
```
### Acquisition des données : côté hardware

C’est la partie importante du projet, pour acquérir les données, nous avons besoin
d’un système embarqué. Ce système embarqué sera réalisé à l’aide du ESP8266 et
un capteur d'intensité du courant ACS712.

Le capteur de courant lie directement avec le compteur, il acquiert les données et
les envoie vers le microcontrôleur, cette dernière envoie les données vers un serveur
Google qui contient notre base de donnée NoSQL.


## 2. Base de donnée NoSQL : Firebase

Nous travaillons avec la base de donnée NoSQL, car elle traite le problème de per-
formance En effet, un des problèmes récurrents des bases de données relationnelles
est la perte de performance lorsque l'on doit traiter un très gros volume de données
De plus, la multiplication des architectures distribués a apporté le besoin de disposer
de solution s'adaptant nativement aux mécanismes de réplication des données et de
gestion de la charge.

### Google Firebase

Google Firebase est une plateforme mobile créée en 2011 par James Tamplin et
Andrew Lee, puis rachetée par Google en 2014 pour être intégrée à leur offre de ser-
vices Cloud (Google Cloud Platform). L'objectif premier de Firebase est de vous
libérer de la complexité de création et de la maintenance d'une architecture serveur,
tout en vous garantissant une scalabilité à toute épreuve (plusieurs milliards d'utili-
sateurs) et une simplicité dans l'utilisation.

Pour cela, Firebase a été décomposée en plusieurs produits extrêmement riches et
adaptés au monde du mobile.

### Base de donnée firebase en temps réel

Google firebase offre une base de donnée en temps réel qui travaille avec les docu-
ments, alors notre projet utilise cette base de donnée avec la structure suivante :


## 3. Système électronique

Ce chapitre traite la première partie de notre projet, qui consiste d’un système d'ac-
quisition de taux de consommation d’énergie électrique, et l'envoyer par réseaux
pour le stockées dans un serveur de base des données NoSQL.

### Acquisition de taux de consommation d'énergie électrique.................................

Au Maroc le réseau électrique délivre l'énergie avec un courant alternatif d’une fré-
quence de 50Hz, et sous une valeur efficace de tension de 220V.

Pour calculer le taux de consommation, il faut d’abord calculer la puissance, qui est
égale la multiplication de la valeur efficace du tension et d’intensité, et le multiplier
par le temps de consommation.

```
P = U x I
E = P x T
```
Où :

```
 U : valeur efficace du tension (V)
 I : valeur efficace de l’intensité (A)
```

```
 P : puissance active (W)
 T : temps de consommation (h)
 E : énergie consommé (Wh)
```
Notre idée a été l'acquisition des deux valeurs, la première est la valeur efficace de
la tension, et la deuxième est la valeur efficace d’intensité, cela est pour un calcule
précis de la puissance électrique au niveau de point de consommation. Pour avoir
réalisé cela on a besoin soit, d’un capteur de puissance (PZEM-004T par exemple),
ou bien un capteur pour la tension (ZMPT101B) et un autre pour l’intensité du cou-
rant (ACS712).

A cause de la pandémie mondiale, les marchés sont affectés par la clôture des fron-
tières, et comme solution, nous avons figuré d'utiliser seulement un capteur d’in-
tensité du courant qui a la référence ACS712 avec une limite maximale d’intensité
de 30A. Et prendre la valeur efficace de la tension comme un constant fixe sur
220V.

#### Le ACS712 : capteur d’intensité du courant

Le ACS712 est un capteur d’intensité du courant électrique, il fonctionne sous le
principe d’effet de Hall. L’effet de Hall se résume par la phrase :

« Un courant électrique traversant un matériau baignant dans un champ magné-
tique, engendre une tension perpendiculaire à ce dernier »

Dans le ACS712 le courant qu'on veut mesurer crée un champ magnétique, ce der-
nier affecte un matériau à l'intérieur du capteur, cette influence génère une tension
proportionnelle à notre courant selon une fonction linéaire.

Le capteur prend dans Vcc une tension maximale de 5V pour l’alimentation, et dans
Vout il fait sortir la tension proportionnelle à l'intensité du courant passé par les
entrées du capteur.

### Envoi des données au serveur NoSQL

Dans le IoT y a pas mal des solutions peut résoudre ce type des problèmes, pour
notre preuve de concept, on a choisi l’utilisation du carte Nodemcu qui se compose
d’une ESP8266, et un port série à USB pour communiquer avec l’ordinateur.

#### Nodemcu (ESP8266)

La Nodemcu est une carte électronique de développement programmable qui se
base sur la carte ESP8266, cette dernière à un microcontrôleur 32 bit qui est puissant
avec une fréquence de 80 MHz, une SRAM de 64 Kb et une mémoire flash de 4
Mb, il vous permet de gérer différents capteurs, il a un convertisseur analogique
vers numérique et il se connecte aussi au réseaux wifi.

#### Partage des données avec le serveur

Le capteur ACS712 discuté précédemment, a une sortie analogique (la tension pro-
portionnelle au courant) ce qui demande de le convertir en une donnée numérique
pour l'envoyer vers le serveur.


La communauté d’arduino ont fourni des librairies simplifiant la programmation, la
librairie ACS712 qui permet d’extraire la valeur mesuré d’intensité à partir du cap-
teur. Il y a aussi la librairie Firebase Arduino qui vous permet de se connecter et
sauvegarder des données à partir du ESP8266, dans une base des données Firebase.

### Implémentation

D'après toutes les technologies et outils décrits précédemment, on peut avoir un
système qui doit répondre aux besoins de notre cahier des charges.

#### Schéma électrique

Notre schéma électrique qui combine tous les outils proposé est résumé par la figure
suivante :


Pour l'implémentation d'une preuve de concept on a utilisé une lampe de type LED
de 9W.

Le ACS712 se monte en série avec la charge (les diapositives consomment l'éner-
gie), sa sortie est montée dans le pin A0 du convertisseur analogique vers numé-
rique du ESP8266, aussi il prend son alimentation depuis le régulateur de
l'ESP8266.

L’ESP8266 est alimenté par une source d’alimentation de 5V, et connecté au réseau
wifi pour envoyer les données au serveur Firebase.


#### Programmtion

La programmation du microcontrôleur de l’ESP8266 peut être faite par le logiciel
Arduino IDE, qui est un logiciel gratuit dédié à programmer les microcontrôleurs
avec un langage similaire au langage C qui s'appelle le MicroC. Après l’installation
de l’IDE, en installe les pilotes compatibles avec votre carte de développement,
pour le port série en USB, puis il faut installer les bibliothèques pour la ESP8266,
ses bibliothèques contient un ensemble des fonctions et classes pour exploiter toute
les fonctionnalités de notre microcontrôleur.

Notre code se compose en trois parties, la première est de se connecter à un réseau
WiFi local pour accéder à l’internet. La deuxième partie consiste à lire et convertir
les données du capteur. La troisième partie est d'envoyer les données extraites vers
le serveur de base des données.



## 4. Application Mobile : côté clients

L’application mobile sert à fournir aux client une vue globale sur leurs consomma-
tions d’électricité. La technologie utilisée pour construire l’application est Ionic,
donc c’est quoi l’Ionic? et c’est quoi la philosophie (la logique) de l’application?

### Ionic : pour construire des applications mobiles hybrides

Ionic est un Framework pour développer des applications mobiles avec HTML,
CSS et JavaScript. Les applications ioniques s'exécutent en tant qu'applications na-
tives et ont une apparence naturelle.

Ionic est basé sur le Framework AngularJS et fournit une solution complète pour
concevoir, construire et conditionner des applications mobiles. La conception est
réalisée à l'aide d'une collection d'outils de modèles et d'une bibliothèque d'icônes
personnalisée. Ionic fournit des composants CSS / SASS personnalisés ainsi que
des extensions d'interface utilisateur Javascript. Les applications ioniques peuvent
être créées, émulées et intégrées à leur interface de ligne de commande (CLI).

Les modèles ioniques sont dynamiques et réactifs et s'adaptent à leur environnement
pour fournir une apparence naturelle. Cette adaptation comprend la disposition, le
style et les icônes. Ionic rend également possible la personnalisation de plate-forme
indépendante. Les applications ioniques utilisant la technologie Web frontale, elles
peuvent également être affichées dans un navigateur pour un développement plus
rapide.

Les applications ioniques sont construites sur Apache Cordova par défaut. Ils ont
accès à tous les plug-ins Cordova qui vous permettent d'utiliser des fonctionnalités
natives, telles que les notifications push, les caméras, les accéléromètres, etc. Les


applications Cordova fonctionnent sur plusieurs plateformes et périphériques Cor-
dova peut être remplacé par d’autres technologies multi plateformes telles que trig-
ger.io.

### Installation du Ionic

Il faut avoir une version à jour de Node.js installée sur notre système. Nous ouvrons
une fenêtre de commande (Windows) et nous installons Ionic à l’aide de la com-
mande suivante:

```
 npm install -g @ionic/cli
```
### La logique de notre application.

En effet, nous utilisons la base de donnée NoSQL juste pour le stockage. La logique
de calcul de consommation est au niveau d’application, donc comme nous l'avons
mentionné précédemmen que notre application contient 4 onglets, l’un pour login,
autres pour accueil, paramètre et about.

Les onglets qui ont besoin un peu de réflexion et peu de travail sont les onglets de
login et d’accueil car ils ont une liaison directe avec la base de donnée NoSQL, au
contraire pour l’onglet ‘about’, il contient que des composants statiques.

### Les onglets de l’application


### Mode nuit

On pourrait penser que le « mode nuit » est réservé, comme son nom l'indique, à
une utilisation nocturne. Sur Twitter, par exemple, on peut l'activer en appuyant sur
une icône en forme de lune. L'option s'avère pourtant populaire de jour comme de
nuit. Ses utilisateurs et défenseurs (telle l'autrice de ces lignes) revendiquent que le
« mode sombre » épargne leurs yeux, mais aussi la durée de vie de la batterie de
leur smartphone, moins usée qu'avec l'exploitation d'applications très lumineuses.
C'est particulièrement vrai pour les téléphones les plus récents, dotés d'un écran
OLED


### Onglet de login

Ionic offre la possibilité de séparer la logique de l’application au vue (view). Nous
utilisons le langage HTML et SCSS pour construire la partie ‘vue’ de chaque onglet,
et nous utilisons le langage Typescript pour construire sa partie ‘logique’.

Cet onglet contient deux inputs : l’un pour identifiant de l’utilisateur, et l’autre pour
le mot de passe. Le Typescript de cet onglet parcours tous les utilisateurs qui se
résident dans la base de donnée, et il fait le test de correspondance, si l’utilisateur
dans la base de donnée alors, nous transférons vers le prochain onglet : Accueil avec
une clé qui nous aide à extraire les données de cet utilisateur qui vient de s’authen-
tifier Sinon l’application va afficher un message d’erreur.


### Onglet Accueil

C’est l’onglet plus intéressent, il contient les informations sur le courant, la con-
sommation d’aujourd’hui, la consommation mensuelle ainsi la facture. Ce qui con-
cerne la partie courant, nous prenons la dernière valeur obtenu dans la base de don-
née pour l’utilisateur connecté. Pour calculer le taux de consommation d’un jour
donné, il faut d’abord calculer la puissance, qui est égale la multiplication de la
valeur efficace du tension et d’intensité, et le multiplier par le temps de consomma-
tion.

Nous extrairons ces informations à partir de la base de donnée. La valeur d’intensité
est la moyenne de toutes les valeurs qui résident dans la base de donnée. Le temps
de consommation est le nombre des documents enregistrés dans la base de donnée,
par exemple (si nous avons 30 enregistrements alors le compteur a consommé 30
minutes de temps). La valeur de tension est fixée dans 220v.

Pour le calcul du mois, il a le même principe qu’un jour donné, mais cette fois-ci
nous extrairons toutes les données d’un mois donné (le mois actuel) et ce n’ai pas
d’un seul jour.

Pour calculer le montant, nous multiplions la valeur obtenu dans la consommation
mensuelle et 1.0732. (Y a la possibilité de travailler avec les tranches)

### Onglet paramètre

Cet onglet donne la possibilité au utilisateur connecté de changer son mot de passe.
En fait l’application enregistre la clé de l’utilisateur courant lors d’authentification,
alors les mise-à-jour font à l’aide de cette clé.

L’onglet offre aussi la possibilité de changer les préférences de couleurs, ces chan-
gements de couleur sont faites à l’aide d’une variable au niveau des schémas de
couleurs « _à voir l’architecture d’Angular_! ».

Nous ne donnons pas la possibilité de se changer l’identifiant car il a une relation
directe avec le système électronique (Le système envoie les données à l’aide de cet
identifiant, quand il change, le système tombe en panne).


### Onglet About

Un onglet qui contient que des éléments statiques. Par exemple l’objectif du projet,
les développeurs du projet, les taches qui ont effectué etc.
